#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n;
    cin >> n;
    vector<vector<int>> dp(n, vector<int>(n));
    rep(i,(n+1)/2) {
        rep(j,n-2*i) {
            dp[i][j+i] = i;
            dp[j+i][i] = i;
            dp[n-1-i][j+i] = i;
            dp[j+i][n-1-i] = i;
        }
    }

    vector<int> p(n*n);
    rep(i,n*n) {
        cin >> p[i];
        p[i]--;
    }

    auto tra = [n](int x) {
        int i, j;
        j = (x % n);
        i = (x - j) / n;
        return P(i,j);
    };

    int x, y, xx, yy;
    int dir[4][2] = {{-1,0},{1,0},{0,1},{0,-1}};
    int ans = 0;
    rep(i,n*n) {
        x = tra(p[i]).first;
        y = tra(p[i]).second;
        int minp = INF;
        for (auto d: dir) {
            xx = x + d[0];
            yy = y + d[1];
            if (yy < 0 || xx < 0 || yy >= n || xx >= n) continue;
            minp = min(minp,dp[xx][yy]);
            dp[xx][yy] = min(dp[x][y], dp[xx][yy]);
        }
        ans += dp[x][y];
    }
    cout << ans << endl;
    
    return 0;
}