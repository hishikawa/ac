#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

ll powx(ll x, ll n) {
    ll a = 1;
    rep(i,n) {
        if (LLINF/x < a) continue;
        a *= x;
    }
    return a;
}

int main() {
    fast_io
    int t;
    cin >> t;
    ll n, a, b, c, d;
    ll amax = 70, bmax = 50, cmax = 40;
    ll at, bt, ct, total;
    // ll dn;
    ll ac, bc, cc, dc, tc;
    vector<ll> ans(t, LLINF);
    // int aa[3] = {-1, 0, 1};
    // int bb[5] = {-2, -1, 0, 1, 2};
    // int cc[9] = {-4, -3, -2, -1, 0, 1, 2, 3, 4};
    rep(l,t){
        cin >> n >> a >> b >> c >> d;
        rep(i,amax) {
            at = powx(2,i);
            if (LLINF / 3 < at) break;
            ac = i * a;
            rep(j,bmax) {
                bt = powx(3,j);
                if (LLINF / at < bt) break;
                bc = j * b;
                rep(k,cmax) {
                    ct = powx(5,k);
                    if (LLINF / at / bt < ct) break;
                    cc = k * c;
                    total = at * bt * ct;
                    if ((abs(n-total) + 1) > (INF / d)) continue;
                    dc = (abs(n-total) + 1) * d;
                    tc = ac + bc + cc + dc;
                    ans[l] = min(ans[l], tc);
                }
            }
        }
    }
    rep(i,t) cout << ans[i] << endl;
    return 0;
}