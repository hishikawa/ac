#include <iostream>
#include <vector>
#define rep(i,n) for (int i = 0; i < n; ++i)

using namespace std;

vector<vector<int>> to;
vector<int> ans;

void dfs(int cur, int parent=-1) {
    for (int child: to[cur]) {
        if (child == parent) continue;
        ans[child] += ans[cur];
        dfs(child, cur);
    }
}


int main() {
    int n, q;
    cin >> n >> q;

    to = vector<vector<int>>(n);
    int a, b;
    rep(i,n-1) {
        cin >> a >> b;
        a--; b--;
        to[a].push_back(b);
        to[b].push_back(a);
    }

    ans = vector<int>(n);
    int p, x;
    rep(i,q) {
        cin >> p >> x;
        p--;
        ans[p] += x;
    }

    dfs(0);
    rep(i,n) cout << ans[i] << " ";

    return 0;
}