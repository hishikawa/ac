#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n;
    cin >> n;

    ll i = 1, count = 0;
    while (n > 0) {
        i *= 26;
        n -= i;
        count++;
    }

    n += i - 1;

    vector<int> p(count);
    // ll div = 1;
    // rep(j,count) {
    //     div *= 26;
    // }

    // rep(j,count) {
    //     p[j] = n / div;
    //     div /= 26;
    //     n %= div;
    // }

    rep(j,count) {
        p[j] = n % 26;
        n /= 26;
    }

    rep(i, count) {
        ll ind = count - i - 1;
        char out;
        // if (p[ind] == 0) {out = 'z';}
        // else out = 'a' + p[ind];
        out = 'a' + p[ind];
        cout << out;
    }
    cout << endl;
    return 0;
}