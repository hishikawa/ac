#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n;
    cin >> n;

    map<int,int> mp;
    ll total = 0;
    rep(i,n) {
        int a;
        cin >> a;
        mp[a]++;
        total += a;
    }

    int q;
    cin >> q;
    vector<P> que(q);
    rep(i,q) cin >> que[i].first >> que[i].second;

    rep(i,q) {
        int b = que[i].first, c = que[i].second;
        total = total + mp[b] * (c - b);
        mp[c] += mp[b];
        mp[b] = 0;
        cout << total << endl;
    }

    return 0;
}