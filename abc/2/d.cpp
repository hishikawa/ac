#include <iostream>
#include <vector>
#include <algorithm>
#define rep(i,n) for (int i = 0; i < n; ++i)

using namespace std;

int main() {
    int n, m;
    cin >> n >> m;

    vector<pair<int,int>> v;
    int x, y;
    rep(i,m) {
        cin >> x >> y;
        --x; --y;
        v.emplace_back(x, y);
    }

    int cmb, people;
    int ans = 1;
    rep(i,1<<n) {
        cmb = 0;
        people = 0;
        rep(k,n) if (i >> k & 1) people++;
        if (people <= 1) continue;
        rep(j,m) {
            if (((i>>v[j].first) & 1) && ((i>>v[j].second) & 1)) cmb++;
        }
        if (cmb == people*(people-1)/2) ans = max(ans, people);
    }
    cout << ans << endl;
    return 0;
}