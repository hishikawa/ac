#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

ll gcd(ll a, ll b) {
    ll r;
    if (!a || !b) return 0LL;
    if (a < b) {
        r = (b % a);
        if (!r) return a;
    } else {
        r = (a % b);
        if (!r) return b;
    }
    return gcd(b, r);
}
int gcd(int a, int b) {
    int r;
    if (!a || !b) return 0LL;
    if (a < b) {
        r = (b % a);
        if (!r) return a;
    } else {
        r = (a % b);
        if (!r) return b;
    }
    return gcd(b, r);
}

int main() {
    fast_io
    ll a, b;
    cin >> a >> b;
    ll x = gcd(a,b), x_copy = x;
    int ind;
    map<ll,int> mp;
    for (ll i = 2; i*i < x_copy; i++) {
        if (x % i) continue;
        ind = 0;
        while (x % i == 0) {
            x /= i;
            ind++;
            mp[i]++;
        }
        if (x == 1) break;
    }
    if (x != 1) mp[x]++;
    ll ans = 0;
    for (auto m: mp) ans++;
    cout << ans + 1 << endl;
    return 0;
}