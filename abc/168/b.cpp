#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <cstdio>
#define rep(i,n) for (int i = 0; i < n; ++i)
const int INF = 1001001001;

using namespace std;

int main() {
    int k;
    cin >> k;
    string s;
    cin >> s;

    if (s.size() <= k) {
        cout << s << endl;
    } else {
        rep(i,k) {
            cout << s[i];
        }
        cout << "..." << endl;
    }
    return 0;
}