#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <cstdio>
#include <map>
#include <numeric>
#define rep(i,n) for (int i = 0; i < n; ++i)
const int INF = 1001001001;
const long long INF = 1000000007;

using namespace std;

int main() {
    int n;
    cin >> n;
    long long a, b, g, tmp;
    long long zero = 0;
    bool rot;
    double grad;
    map<pair<long long,long long>,pair<int,int>> mp;
    rep(i,n) {
        cin >> a >> b;
        if (a == 0 && b == 0) {
            zero++;
            continue;
        }
        if (b < 0) a = -a, b = -b;
        rot = (a <= 0);
        if (a <= 0) tmp = a, a = b, b = -tmp;
        g = gcd(a,b);
        a /= g;
        b /= g;

        if (rot) mp[{a,b}].first++;
        else mp[{a,b}].second++;
    }
    
    long long ans = 0;
    int s, t;
    for (auto gr: mp) {
        s = gr.second.first;
        t = gr.second.second;
        ans += gr.second.first
    }
    return 0;
}