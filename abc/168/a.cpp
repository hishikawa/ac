#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <cstdio>
#define rep(i,n) for (int i = 0; i < n; ++i)
const int INF = 1001001001;

using namespace std;

int main() {
    string n;
    cin >> n;
    int l = n.size();
    int x = n[l-1] - '0';
    if (x == 3) cout << "bon" << endl;
    else if (x == 0 || x == 1 || x == 6 || x == 8) cout << "pon" << endl;
    else cout << "hon" << endl;
    return 0;
}