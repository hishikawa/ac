#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <cstdio>
#define rep(i,n) for (int i = 0; i < n; ++i)
const int INF = 1001001001;
const double PI = 3.14159265358979323846;

using namespace std;

int main() {
    int a, b, h, m;
    cin >> a >> b >> h >> m;

    double hx, hy, mx, my;
    hx = a * cos((h*60.0 + m) *2*PI / (60.0*12));
    hy = a * sin((h*60.0 + m) *2*PI / (60.0*12));
    mx = b * cos(m*2*PI/60);
    my = b * sin(m*2*PI/60);

    double dx, dy;
    dx = hx - mx;
    dy = hy - my;

    double dist = sqrt(dx*dx + dy*dy);

    printf("%.11f\n", dist);
    return 0;
}