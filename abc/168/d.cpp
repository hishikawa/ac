#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <cstdio>
#include <queue>
#define rep(i,n) for (int i = 0; i < n; ++i)
const int INF = 1001001001;

using namespace std;

int main() {
    int n, m;
    cin >> n >> m;
    vector<vector<int>> to(n);
    int a, b;
    rep (i,m) {
        cin >> a >> b;
        --a; --b;
        to[a].push_back(b);
        to[b].push_back(a);
    }

    queue<int> q;
    q.push(0);
    vector<bool> seen(n, false);
    seen[0] = true;
    vector<int> ans(n);

    int cur;
    while (!q.empty()) {
        cur = q.front();
        q.pop();
        for (int next: to[cur]) {
            if (seen[next]) continue;
            else {
                ans[next] = cur;
                seen[next] = true;
                q.push(next);
            }
        }
    }
    
    cout << "Yes" << endl;
    rep(i,n-1) cout << ans[i+1]+1 << endl;
    return 0;
}