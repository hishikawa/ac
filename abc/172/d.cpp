#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n;
    cin >> n;
    ll ans = 0;
    vector<bool> seen(n+1, false);
    rep(j,n) {
        if (seen[j]) continue;
        ll x = j+1;
        if (x % 1000000 == 0) cout << x << endl;
        map<ll,ll> prime;
        ll x_tmp = x;
        for (ll i = 2; i * i <= x; i++) {
            if (x_tmp % i) continue;
            while (x_tmp % i == 0) {
                x_tmp /= i;
                prime[i]++;
            }
        }
        if (x_tmp != 1) prime[x_tmp]++;
        ll ans_tmp = 1;
        for (auto p_num: prime) {
            ll num = p_num.second;
            ans_tmp *= num + 1;
        }
        ans += ans_tmp * x;
    }
    cout << ans << endl;
    return 0;
}