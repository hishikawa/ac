#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n, m, k;
    cin >> n >> m >> k;
    vector<ll> a(n), b(m);
    rep(i,n) cin >> a[i];
    rep(i,m) cin >> b[i];

    rep(i,n-1) a[i+1] += a[i];
    rep(i,m-1) b[i+1] += b[i];

    // aを０冊読むとき
    ll a_count = 0;
    ll time_tmp = 0;
    ll time_rest = k - time_tmp;
    ll b_count = upper_bound(b.begin(), b.end(), time_rest) - b.begin();
    ll count = a_count + b_count;

    // aをi+1札読むとき
    rep(i,n) {
        a_count = i+1;
        time_tmp = a[i];
        time_rest = k - time_tmp;
        if (time_rest < 0) break;
        b_count = upper_bound(b.begin(), b.end(), time_rest) - b.begin();
        ll count_tmp = a_count + b_count;
        count = max(count, count_tmp);
    }
    
    
    cout << count << endl;
    
    return 0;
}