#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

ll score(char rsp, int r, int s, int p) {
    if (rsp == 'r') return p;
    else if (rsp == 's') return r;
    else return s;
}

int main() {
    fast_io
    int n, k, r, s, p;
    string t;
    cin >> n >> k >> r >> s >> p >> t;
    ll total = 0;
    rep(i,n) {
        if (i >= k) {
            if (t[i-k] == t[i]) {
                t[i] = '#';
                continue;
            }
        }
        total = total + score(t[i], r, s, p);
    }
    cout << total << endl;
    return 0;
}