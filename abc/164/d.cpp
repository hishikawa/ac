#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <stdio.h>

using namespace std;

int main() {
    string s;
    cin >> s;

    int digits = s.size();

    long long ans;
    ans = 0;

    vector<int> dp(digits);
    vector<int> a(digits);

    int sum = 0;
    int mul = 1;
    for (int i = 0; i < digits; ++i) {
        a[i] = (s[digits - i - 1] - '0') * mul;
        a[i] %= 2019;
        sum += a[i];
        sum %= 2019;
        dp[i] = sum;
        mul = ((mul*10) % 2019);
    }

    map<int, int> mp;
    mp[0]++;
    for (int i = 0; i < digits; i++) {
        ans += mp[dp[i]];
        mp[dp[i]]++;
    }
    
    cout << ans << endl;
    return 0;
}