#include <iostream>

using namespace std;

int main() {
    int a, b, c, d;
    cin >> a >> b >> c >> d;

    while (a > 0 && c > 0){
        a -= d;
        c -= b;
    }
    if (c < 1) cout << "Yes" << endl;
    else cout << "No" << endl;
    return 0;
}