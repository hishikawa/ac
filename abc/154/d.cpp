#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n, k;
    cin >> n >> k;
    vector<double> p(n+1);
    p[0] = 0;
    double ptmp;
    rep(i,n) {
        cin >> ptmp;
        p[i+1] = ((ptmp + 1.0) / 2.0);
    }
    rep(i,n) p[i+1] += p[i];
    double ans = 0;
    rep(i,n-k+1) ans = max(ans, p[i+k] - p[i]);
    printf("%.10f\n", ans);

    return 0;
}  