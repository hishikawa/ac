#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    string s;
    int k;
    cin >> s >> k;
    // 桁数
    int n_digits = s.size();
    // dp[i][smaller][j]
    // 大きい桁からi番目までに注目 0 <= i <= n_digits
    // その時そのi番目までの数が、sのi番目までの桁と一致しているか、それともそれより小さいか
    // sより大きい場合は見なくてよい
    // その時非ゼロである桁がjこあるとする
    // 以上の条件を満たす数
    const int MAX_DIGITS = 105, SMALLER = 2, MAX_K = 4;
    ll dp[MAX_DIGITS][SMALLER][MAX_K];
    rep(i,MAX_DIGITS) rep(j,SMALLER) rep(l,MAX_K) dp[i][j][l] = 0;
    dp[0][0][0] = 1;
    rep(i,n_digits)  rep(l,k) rep(smaller,SMALLER){
        // sの次の桁
        int true_d = (s[i] - '0');
        // 次の桁がtemp_dの時を考える
        rep(temp_d,10) {
            int ns = smaller, nl = l;
            // もし次の桁が０でないなら
            if (temp_d != 0) nl+=1;
            // もし次の桁の非ゼロの数がMAX_Kを超えるなら
            if (nl > k) continue;
            // もし現在までのけたがｓと一致してるなら
            if (smaller == 0) {
                // 次の桁が実際の次の桁より大きいとき
                if (temp_d > true_d) continue;
                // 次のけたが実際の次の桁より小さいとき
                if (temp_d < true_d) ns = 1;
            }
            dp[i+1][ns][nl] += dp[i][smaller][l];
        }
    }
    ll ans = (dp[n_digits][1][k] + dp[n_digits][0][k]);
    cout << ans << endl;
    return 0;
}