#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n = 6;
    vector<int> v(n);
    vector<int> a(n);
    rep(i,n) {
        v[i] = n-1-i;
    }
    a[0] = 5;
    a[1] = 5;
    a[2] = 6;
    a[3] = 1;
    a[4] = 1;
    a[5] = 1;
    int happy, ans = 0;
    vector<int> abss(n);
    do {
        happy = 0;
        rep(i,n) {
            happy += abs(i - v[i]) * a[v[i]];
        }
        ans = max(happy, ans);
        if ( happy == 58 ) {
            rep(i,n) cout << v[i] << endl;
            cout << endl;
            break;
        }
    } while (prev_permutation(v.begin(), v.end()));
    cout << ans << endl;
    return 0;
}