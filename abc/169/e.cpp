#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n;
    cin >> n;
    vector<P> ab, ba;
    int a,b;
    rep(i,n) {
        cin >> a >> b;
        ab.emplace_back(a,b);
        ba.emplace_back(b,a);
    }
    sort(ab.begin(),ab.end());
    sort(ba.begin(),ba.end());
    int center_l, center_r;
    if (n % 2) {
        center_l = center_r = n / 2;
    } else {
        center_l = n / 2 - 1;
        center_r = n / 2;
    }
    int max_l = 0;
    for (int i = 0; i <= center_l; i++) {
        max_l = max(max_l, ab[i].second);
    }
    int min_l = ab[center_l].first;
    int min_r = INF;
    for (int i = n-1; i >= center_r; i--) {
        min_r = min(min_r, ba[i].second);
    }
    int max_r = ba[center_r].first;
    int ans1, ans2, ans;
    // if (n % 2) {
        ans1 = max(min_l, min_r);
        ans2 = min(max_l, max_r);
        ans = max(0, ans2 - ans1 + 1);
    // }
    cout << ans << endl;
    return 0;
}