#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n;
    cin >> n;
    ll ans = 1;
    ll a;
    bool flag = false, flag2 = false;
    rep(i,n) {
        cin >> a;
        if (a == 0){ flag2 = true; continue;}
        if (ans > 1000000000000000000 / a) {flag = true;}
        ans *= a;
    }
    if (flag2) {cout << 0 << endl; return 0;}
    else if (flag) {cout << -1 << endl; return 0;}
    else cout << ans << endl;
    return 0;
}