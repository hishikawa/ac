#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n;
    cin >> n;
    map<ll,ll> mp;
    for (ll i = 2; i * i < n; i++) {
        if (n % i) continue;
        while (n % i == 0) {
            n /= i;
            mp[i]++;
        }
    }
    if (n != 1) mp[n]++;

    ll ans = 0;
    for (auto m: mp) {
        ll x = m.second;
        int j = 1;
        while (x >= j) {
            x -= j;
            j++;
            ans++;
        }
    }
    cout << ans << endl;
    return 0;
}