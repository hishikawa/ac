#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll a;
    string b;
    cin >> a >> b;
    double b1, b2, b3;

    b1 = b[0] - '0';
    b2 = b[2] - '0';
    b3 = b[3] - '0';

    b1 = (b1 * a);
    b2 = (b2 * a);
    b3 = (b3 * a) / 10.0;
    b2 = b2 + b3;
    b2 = b2 / 10.0;
    
    ll ans = 0;
    ans = b1 + b2;
    cout << ans << endl;
    return 0;
}