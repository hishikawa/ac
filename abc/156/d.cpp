#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

class Mint {
    ll a;
public:
    void set(ll a_) {a = a_ % MOD;}
    void set(int a_) {a = a_ % MOD;}
    ll get() {return a;}
    Mint(ll a_) {a = (a_ % MOD);}
    Mint(int a_) {a = (a_ % MOD);}
    Mint() {a = 1;}
    // 演算子定義
    inline Mint& operator =(const Mint &b);
    inline Mint& operator =(const int &b);
    inline Mint& operator =(const ll &b);
    inline Mint& operator +=(const Mint &b);
    inline Mint& operator +=(const int &b);
    inline Mint& operator +=(const ll &b);
    inline Mint& operator *=(const Mint &b);
    inline Mint& operator *=(const int &b);
    inline Mint& operator *=(const ll &b);
    inline Mint& operator -=(const Mint &b);
    inline Mint& operator -=(const int &b);
    inline Mint& operator -=(const ll &b);
    inline Mint operator +(const Mint &b) const;
    inline Mint operator +(const int &b) const;
    inline Mint operator +(const ll &b) const;
    inline Mint operator *(const Mint &b) const;
    inline Mint operator *(const int &b) const;
    inline Mint operator *(const ll &b) const;
    inline Mint operator -(const Mint &b) const;
    inline Mint operator -(const int &b) const;
    inline Mint operator -(const ll &b) const;
    inline bool operator ==(const Mint &b) const;
    inline bool operator ==(const int &b) const;
    inline bool operator ==(const ll &b) const;
    inline bool operator !=(const Mint &b) const;
    inline bool operator !=(const int &b) const;
    inline bool operator !=(const ll &b) const;
    inline bool operator <=(const Mint &b) const;
    inline bool operator <=(const int &b) const;
    inline bool operator <=(const ll &b) const;
    inline bool operator <(const Mint &b) const;
    inline bool operator <(const int &b) const;
    inline bool operator <(const ll &b) const;
    inline bool operator >=(const Mint &b) const;
    inline bool operator >=(const int &b) const;
    inline bool operator >=(const ll &b) const;
    inline bool operator >(const Mint &b) const;
    inline bool operator >(const int &b) const;
    inline bool operator >(const ll &b) const;
    inline friend ostream& operator<<(ostream& os, const Mint& b);
    inline friend istream& operator>>(istream& is, const Mint& b);
    // 累乗計算 計算量O(logn)
    inline Mint pow(const int &x) const;
    // MODが素数の時、逆元を計算 計算量O(log(MOD))
    Mint inv() const;
    // MODが素数の時、nCiの要素v[i]をもつvectorを返す
    inline vector<Mint> cmbv(const int &n) const;
    // MODが素数の時、nCiを返す
    inline Mint cmb(const int &n, const int &m) const;
};


Mint& Mint::operator =(const Mint &b) {
    a = b.a;
    return *this;
}
Mint& Mint::operator =(const int &b) {
    a = (b % MOD);
    return *this;
}
Mint& Mint::operator =(const ll &b) {
    a = (b % MOD);
    return *this;
}
Mint& Mint::operator +=(const Mint &b) {
    a += b.a;
    a %= MOD;
    return *this;
}
Mint& Mint::operator +=(const int &b) {
    a += (b % MOD);
    a %= MOD;
    return *this;
}
Mint& Mint::operator +=(const ll &b) {
    a += (b % MOD);
    a %= MOD;
    return *this;
}
Mint& Mint::operator *=(const Mint &b) {
    a *= b.a;
    a %= MOD;
    return *this;
}
Mint& Mint::operator *=(const int &b) {
    a *= (b % MOD);
    a %= MOD;
    return *this;
}
Mint& Mint::operator *=(const ll &b) {
    a *= (b % MOD);
    a %= MOD;
    return *this;
}
Mint& Mint::operator -=(const Mint &b) {
    a -= b.a;
    a %= MOD;
    if (a < 0) a += MOD;
    return *this;
}
Mint& Mint::operator -=(const int &b) {
    a -= (b % MOD);
    a %= MOD;
    if (a < 0) a += MOD;
    return *this;
}
Mint& Mint::operator -=(const ll &b) {
    a -= (b % MOD);
    a %= MOD;
    if (a < 0) a += MOD;
    return *this;
}
Mint Mint::operator +(const Mint &b) const {
    Mint c;
    c.a = ((this->a + b.a) % MOD);
    return c;
}
Mint Mint::operator +(const int &b) const{
    Mint c;
    c.a = ((this->a + (b%MOD)) % MOD);
    return c;
}
Mint Mint::operator +(const ll &b) const{
    Mint c;
    c.a = ((this->a + (b%MOD)) % MOD);
    return c;
}
Mint Mint::operator *(const Mint &b) const{
    Mint c;
    c.a = ((this->a * b.a) % MOD);
    return c;
}
Mint Mint::operator *(const int &b) const{
    Mint c;
    c.a = ((this->a * (b%MOD)) % MOD);
    return c;
}
Mint Mint::operator *(const ll &b) const{
    Mint c;
    c.a = ((this->a * (b%MOD)) % MOD);
    return c;
}
Mint Mint::operator -(const Mint &b) const{
    Mint c;
    int x = this->a - b.a;
    c.a = (x % MOD);
    if (c.a < 0) c.a = c.a + MOD;
    return c;
}
Mint Mint::operator -(const int &b) const{
    Mint c;
    ll x = this->a - (b%MOD);
    c.a = (x % MOD);
    if (c.a < 0) c.a = c.a + MOD;
    return c;
}
Mint Mint::operator -(const ll &b) const{
    Mint c;
    ll x = this->a - (b%MOD);
    c.a = (x % MOD);
    if (c.a < 0) c.a = c.a + MOD;
    return c;
}
bool Mint::operator ==(const Mint &b) const {return a == b.a;}
bool Mint::operator ==(const int &b) const {return a == b;}
bool Mint::operator ==(const ll &b) const {return a == b;}
bool Mint::operator !=(const Mint &b) const {return a != b.a;}
bool Mint::operator !=(const int &b) const {return a != b;}
bool Mint::operator !=(const ll &b) const {return a != b;}
bool Mint::operator <=(const Mint &b) const {return a <= b.a;}
bool Mint::operator <=(const int &b) const {return a <= b;}
bool Mint::operator <=(const ll &b) const {return a <= b;}
bool Mint::operator <(const Mint &b) const {return a < b.a;}
bool Mint::operator <(const int &b) const {return a < b;}
bool Mint::operator <(const ll &b) const {return a < b;}
bool Mint::operator >=(const Mint &b) const {return a >= b.a;}
bool Mint::operator >=(const int &b) const {return a >= b;}
bool Mint::operator >=(const ll &b) const {return a >= b;}
bool Mint::operator >(const Mint &b) const {return a > b.a;}
bool Mint::operator >(const int &b) const {return a > b;}
bool Mint::operator >(const ll &b) const {return a > b;}
ostream& operator <<(ostream& os, const Mint &b) {
    os << b.a;
    return os;
}
istream& operator >>(istream& is, Mint &b) {
    ll c;
    is >> c;
    b.set(c);
    return is;
}

Mint Mint::pow(const int& x) const{
    ll i = 0;
    Mint ans = 1;
    vector<Mint> v;
    v.emplace_back(a);
    while ((x >> i) >= 1) {
        v.emplace_back(v[i]*v[i]);
        if (x >> i & 1) ans *= v[i];
        i++;
    }
    return ans;
}

Mint Mint::inv() const {
    if (this->a == 1) return Mint(1);
    else if (this->a == 0) return Mint(0);
    return this->pow(MOD-2);
}
vector<Mint> Mint::cmbv(const int &n) const {
    vector<Mint> v;
    v.emplace_back(1);
    Mint x, y;
    rep(i,n/2) {
        x = (n - i);
        y = (i + 1);
        v.push_back(v[i] * x * y.inv());
    }
    int size = v.size();
    if (n % 2) rep(i,n/2 + 1) v.push_back(v[size-1-i]);
    else rep(i,n/2) v.push_back(v[size-2-i]);
    return v;
}

Mint Mint::cmb(const int &n, const int &m) const {
    Mint ans = 1;
    Mint x, y;
    rep(i,m) {
        x = (n - i);
        y = (i + 1);
        ans *= x;
        ans *= y.inv();
    }
    return ans;
}

int main() {
    fast_io
    int n, a, b;
    cin >> n >> a >> b;

    Mint ans, cmba, cmbb;
    ans = 2;
    ans = ans.pow(n);
    cmba = cmba.cmb(n, a);
    cmbb = cmbb.cmb(n, b);
    ans -= 1;
    ans -= cmba;
    ans -= cmbb;
    cout << ans << endl;

    return 0;
}