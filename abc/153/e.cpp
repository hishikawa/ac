#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int dfs(int i, int h, const vector<P> &m, vector<vector<int>> &dp) {
    if (h < 0) return 0;
    if (dp[i][h] != INF) return dp[i][h];
    if (i == 0) {
        dp[i][h] = ((h + m[0].first - 1) / m[0].first * m[0].second);
        return dp[i][h];
    }
    dp[i][h] = min(dfs(i-1,h,m,dp), dfs(i,h-m[i].first,m,dp)+m[i].second);
    return dp[i][h];
}

int main() {
    fast_io
    int h, n;
    cin >> h >> n;
    vector<P> m(n);
    rep(i,n) cin >> m[i].first >> m[i].second;
    vector<vector<int>> dp(n, vector<int>(h+1, INF));
    dfs(n-1, h, m, dp);
    cout << dp[n-1][h] << endl;
    return 0;
}