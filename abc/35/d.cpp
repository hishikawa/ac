#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
// typedef long long ll;
// using P = pair<int,int>;
const int MOD = 1000000007;
const int INF = 1001001001;
using namespace std;

// struct Node {
//     int to, cost;
//     Node(int to_, int cost_) {
//         to = to_;
//         cost = cost_;
//     }
// };

int main() {
    int n, m, t;
    cin >> n >> m >> t;
    vector<int> a(n);
    rep(i,n) cin >> a[i];
    int x, y, z;
    vector<vector<pair<int,int>>> to1(n);
    vector<vector<pair<int,int>>> to2(n);
    rep(i,m){
        cin >> x >> y >> z;
        x--; y--;
        // x番目のノードはy番目のノードにつながっていて
        // そのコストはz
        to1[x].emplace_back(z,y);
        // 有効グラフの矢印の向きを逆にしたものも用意
        to2[y].emplace_back(z,x);
    } 

    // i番目に行くための最短距離
    vector<int> dp1(n, INF);
    // i番目から帰るための最短距離
    vector<int> dp2(n, INF);

    priority_queue< pair<int,int>, vector<pair<int,int>>, greater<pair<int,int>> > pq;
    // スタート
    int s = 0;
    dp1[s] = 0;
    pq.emplace(0,s);

    while (!pq.empty()) {
        // 最もコストが小さいノードのコストを確定する
        // firstが累計cost
        // secondがnode番号
        pair<int,int> cur = pq.top();
        pq.pop();
        int cn = cur.second;
        // もしその取り出したコストがすでに別の経路で更新済みだったら
        if (dp1[cn] < cur.first) continue;
        // 確定したノードの周りを見てコストを更新
        for (auto next: to1[cn]) {
            // もしコストが更新されたら
            // そのノードを次の移動ノード候補に加える
            if (dp1[next.second] > dp1[cn] + next.first) {
                dp1[next.second] > dp1[cn] + next.first;
                pq.emplace(dp1[next.first], next.second);
            }
        }
    }

    priority_queue<pair<int,int>, vector<pair<int,int>>, greater<pair<int,int>>> pq2;
    // スタート
    int s2 = 0;
    dp2[s2] = 0;
    pq2.emplace(0,s2);

    while (!pq2.empty()) {
        // 最もコストが小さいノードのコストを確定する
        // firstが累計cost
        // secondがnode番号
        pair<int,int> cur2 = pq2.top();
        pq2.pop();
        int cn2 = cur2.second;
        // もしその取り出したコストが大きかったら次の更新へ
        if (dp2[cn2] < cur2.first) continue;
        // 確定したノードの周りを見てコストを更新
        for (auto next: to2[cn2]) {
            // もしコストが更新されたら
            // そのノードを次の移動ノード候補に加える
            if (dp2[next.second] > dp2[cn2] + next.first) {
                dp2[next.second] > dp2[cn2] + next.first;
                pq2.emplace(dp2[next.first], next.second);
            }
        }
    }

    int ans = 0;
    int t_tmp, money;
    rep(i,n) {
        t_tmp = dp1[i] + dp2[i];
        money = (t - t_tmp) * a[i];
        ans = max(ans, money);
    }

    cout << ans << endl;



    return 0;
}