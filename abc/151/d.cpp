#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
using namespace std;
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;

bool dis(int w, int h, int nx, int ny) {
    if (nx < 0) return false;
    else if (nx >= w) return false;
    else if (ny < 0) return false;
    else if (ny >= h) return false;
    return true;
}

struct Mp {
    int x, y, c;
    Mp(int x_, int y_, int c_) {
        x = x_;
        y = y_;
        c = c_;
    }
};

int main() {
    fast_io
    int h, w;
    cin >> h >> w;
    vector<string> m(h), m_copy;
    rep(i,h) {
        cin >> m[i];
    }
    m_copy = m;

    int nx, ny, nc;
    int total_c = 0;
    int dir[4][2] = {{0,1},{0,-1},{1,0},{-1,0}};
    queue<Mp> q;
    rep(i,w) {
        rep(j,h) {
            m = m_copy;
            vector<vector<int>> dist(h, vector<int>(w, INF));
            int sx = i, sy = j;
            if (m[sy][sx] == '#') continue;
            while (!q.empty()) q.pop();
            q.emplace(sx, sy, 0);
            dist[sy][sx] = 0;
            m[sy][sx] = '#';
            while (!q.empty()) {
                for (auto d: dir) {
                    nx = q.front().x + d[0];
                    ny = q.front().y + d[1];
                    nc = q.front().c + 1;
                    if (dis(w, h, nx, ny)) {
                        if (m[ny][nx] == '.') {
                            q.emplace(nx, ny, nc);
                            dist[ny][nx] = nc;
                            m[ny][nx] = '#';
                        }
                    }
                }
                q.pop();
            }
            rep(i,h) {
                rep(j,w) {
                    if (dist[i][j] == INF) continue;
                    total_c = max(total_c, dist[i][j]);
                }
            }
        }
    }
    cout << total_c << endl;
    return 0;
}