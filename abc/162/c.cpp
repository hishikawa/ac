#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

// int gcd(int a, int b)
// {
//    if (a%b == 0)
//    {
//        return(b);
//    }
//    else
//    {
//        return(gcd(b, a%b));
//    }
// }


int main() {
    fast_io
    int n, x;
    cin >> n;
    ll ans = 0;
    rep(i,n) {
        rep(j,n) {
            x = gcd(i+1, j+1);
            rep(k,n) {
                ans += gcd(x, k+1);
            }
        }
    }
    cout << ans << endl;
    return 0;
}
