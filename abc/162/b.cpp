#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n;
    cin >> n;
    ll ans = 0;
    for (ll i = 0; i < n; i++) {
        if ((i+1)%3!=0 && (i+1)%5!=0) ans += (i+1);
    }
    cout << ans << endl;
    return 0;
}