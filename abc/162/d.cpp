#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    string s;
    int n;
    cin >> n >> s;
    if (n < 3) {
        cout << 0 << endl;
        return 0;
    }
    map<char,ll> mp;
    rep(i,n) {
        mp[s[i]]++;
    }
    ll count = 0;
    ll c, l, r;
    rep(i,n) {
        c = i;
        rep(j,n) {
            l = i-1-j;
            r = i+1+j;
            if (l < 0 || r > n-1) break;
            if (s[c] != s[l] && s[l] != s[r] && s[r] != s[c]) count++;
        }
    }
    cout << ((mp['R'] * mp['G'] * mp['B']) - count) << endl;
    return 0;
}