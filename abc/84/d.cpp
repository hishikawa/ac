#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    int q;
    cin >> q;
    int n = 100001;
    bool eratos[n];
    rep(i,n) eratos[i] = true;
    eratos[0] = false;
    eratos[1] = false;
    int j;
    rep(i,n) {
        if (eratos[i]) {
            j = i + i;
            while (j < n) {
                eratos[j] = false;
                j += i;
            }
        }
    }

    int dp[n+1];
    dp[0] = 0;
    rep(i,n) {
        dp[i+1] = dp[i];
        if (eratos[i] && eratos[(i+1)/2]) dp[i+1]++;
    }

    int l, r;
    vector<int> ans(q);
    rep(i,q) {
        cin >> l >> r;
        ans[i] = dp[r+1] - dp[l];
    }
    rep(i,q) cout << ans[i] << endl;
    return 0;
}