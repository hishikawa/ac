#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

int main(){
    int n, m;
    cin >> n >> m;
    vector<int> height(n), good_obs(n, 1);
    int a, b;
    int h;

    for (int i = 0; i < n; i++){
        cin >> height[i];
    }

    for (int j = 0; j < m; j++){
        cin >> a >> b;
        a--;
        b--;

        if (height[a] < height[b]){
            good_obs[a] = 0;
        }
        else if(height[a] > height[b]){
            good_obs[b] = 0;
        }
        else{
            good_obs[a] = 0;
            good_obs[b] = 0;
        }
    }

    cout << accumulate(good_obs.begin(), good_obs.end(), 0) << endl;

    return 0;
}