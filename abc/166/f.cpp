#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main(){
    int n;
    vector<int> abc(3);

    cin >> n;
    cin >> abc[0] >> abc[1] >> abc[2];

    vector<pair<int, int>> s;

    for (int i = 0; i < n; ++i) {
        string s_temp;
        cin >> s_temp;
        if (s_temp == "AB") s.emplace_back(0, 1);
        else if (s_temp == "AC") s.emplace_back(0, 2);
        else s.emplace_back(1, 2);
    }

    string ans;
    auto add = [&](int a, int b) {
        abc[a]++;
        abc[b]--;
        ans += 'A' + a;
    };

    s.emplace_back(1, 0);
    for (int i = 0; i < n; ++i){
        if (abc[s[i].first] == 0 && abc[s[i].second] == 0) {
            cout << "No" << endl;
            return 0;
        } else if (abc[s[i].first] == 0) {
            add(s[i].first, s[i].second);
        }
        else if (abc[s[i].second] == 0) {
            add(s[i].second, s[i].first);
        }
        else {
            if (s[i].first == s[i+1].first || s[i].first == s[i+1].second) {
                add(s[i].first, s[i].second);
            } else {
                add(s[i].second, s[i].first);
            }
        }
    }

    cout << "Yes" << endl;
    for (int i = 0; i < n; ++i) {
        cout << ans[i] << endl;
    }

    return 0;
}