#include <iostream>
#include <vector>
#include <numeric>
using namespace std;

int main(){
    int n, k;
    int d;
    int a;
    cin >> n >> k;
    vector<int> people(n, 1);

    for (int i = 0; i < k; i++){
        cin >> d;

        for (int j = 0; j < d; j++){
            cin >> a;
            people[a-1] = 0;
        }
    }
    
    cout << accumulate(people.begin(), people.end(), 0) << endl;
}