#include <iostream>
#include <map>
#include <vector>

using namespace std;

int main(){
    int n;
    cin >> n;
    map<int, int> mp;
    long long ans = 0;

    for (int i = 0; i < n; ++i){
        int a, sa, wa;
        cin >> a;
        sa = i - a;
        wa = i + a;
        ans += mp[sa];
        mp[wa]++;
    }

    cout << ans << endl;
    return 0;
}