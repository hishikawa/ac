#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
const int MOD = 1000000007;
const int INF = 1001001001;
using ll = long long;
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> c(n);
    rep(i,n) cin >> c[i];

    vector<int> dp(n+1, INF);
    dp[0] = -INF;
    rep(i,n) *lower_bound(dp.begin(), dp.end(), c[i]) = c[i];
    int len = n + 1 - (lower_bound(dp.begin(), dp.end(), INF) - dp.begin());

    cout << len << endl;

    return 0;
}