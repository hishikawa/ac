#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n, m;
    cin >> n >> m;
    int start = 1, end;
    rep(i,n-1) start *= 10;
    end = start * 10;
    if (n == 1) {start = 0; end = 10;}

    

    vector<int> s(m), c(m);
    rep(i,m) cin >> s[i] >> c[i];

    bool ok;
    for (int i = start; i < end; ++i) {
        int div;
        ok = true;
        rep(j,m) {
            div = 1;
            rep(k,n-s[j]) div *= 10;
            int digit = ((i / div) % 10);
            if (digit != c[j]) ok = false;
        }
        if (ok) {cout << i << endl; return 0;}
    }
    cout << -1 << endl;
    return 0;
}