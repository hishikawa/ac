#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n, q;
    string s;
    cin >> n >> s >> q;

    vector<set<int>> alp(26);
    rep(i,n) alp[s[i]-'a'].insert(i);

    int type, ind, l, r;
    char c;
    vector<int> ans;
    rep(i,q) {
        cin >> type;
        if (type == 1) {
            cin >> ind >> c; ind--;
            alp[s[ind]-'a'].erase(ind);
            alp[c-'a'].insert(ind);
            s[ind] = c;
        } else {
            cin >> l >> r; l--; r--;
            int ans_tmp = 0;
            rep(j,26) {
                auto lb = alp[j].lower_bound(l);
                if (*lb > r || lb == alp[j].end()) continue;
                ans_tmp++;
            }
            ans.push_back(ans_tmp);
        }
    }
    for (int a: ans) cout << a << endl;
    return 0;
}