#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int a[3][3];
    rep(i,3) {
        rep(j,3) {
            cin >> a[i][j];
        }
    }
    int n;
    cin >> n;
    int b;
    rep(i,n) {
        cin >> b;
        rep(i,3) {
            rep(j,3) {
                if (a[i][j] == b) a[i][j] = INF;
            }
        }
    }
    bool bingo = false;
    if (a[0][1] == a[0][0] && a[0][1] == a[0][2]) bingo = true;
    if (a[1][1] == a[1][0] && a[1][1] == a[1][2]) bingo = true;
    if (a[2][1] == a[2][0] && a[2][1] == a[2][2]) bingo = true;
    if (a[1][0] == a[0][0] && a[1][0] == a[2][0]) bingo = true;
    if (a[1][1] == a[0][1] && a[1][1] == a[2][1]) bingo = true;
    if (a[1][2] == a[0][2] && a[1][2] == a[2][2]) bingo = true;
    if (a[0][0] == a[1][1] && a[1][1] == a[2][2]) bingo = true;
    if (a[0][2] == a[1][1] && a[1][1] == a[2][0]) bingo = true;
    string ans;
    ans = bingo ? "Yes" : "No";
    cout << ans << endl;

    return 0;
}