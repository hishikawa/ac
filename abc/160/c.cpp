#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int k, n;
    cin >> k >> n;
    vector<int> a(n);
    rep(i,n) cin >> a[i];
    sort(a.begin(), a.end());
    int max_dist = 0;
    rep(i,n) {
        if (i == n-1) {
            max_dist = max(max_dist, ((k-a[i]) + a[0]));
            continue;
        }
        max_dist = max(max_dist, a[i+1] - a[i]);
    }
    cout << (k-max_dist) << endl;
    return 0;
}