#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int x;
    cin >> x;
    int ans;
    ans = x / 500;
    ans *= 1000;
    x %= 500;
    ans += (x / 5) * 5;
    cout << ans << endl;

    return 0;
}