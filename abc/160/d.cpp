#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n, m;
    cin >> n;
    m = n;
    int x, y;
    cin >> x >> y;
    vector<vector<P>> to(n);
    rep(i,m-1) {
        to[i].emplace_back(1, i+1);
        to[i+1].emplace_back(1, i);
    }
    to[x-1].emplace_back(1,y-1);
    to[y-1].emplace_back(1,x-1);
    
    vector<int> ans(n);
    rep(i,n) {
        // スタート
        int sn = i;
        // スタートからiまでの最小コスト
        vector<int> dp(n, INF);
        dp[sn] = 0;
        // 次に見るノードとこれまでの累計コストを入れる（昇順）
        priority_queue<P, vector<P>, greater<P>> pq;
        pq.emplace(0, sn);
        P c;
        while (!pq.empty()) {
            // 累計コストが最も小さいノードに移動
            c = pq.top();
            pq.pop();
            // もし現在のノードのコストが古いもので
            // すでにほかの経路で更新済みだったら次のノードへ移動
            if (dp[c.second] < c.first) continue;
            // 隣接ノードを見る
            for (auto n: to[c.second]) {
                // 最小コストが更新できるか確認
                if (dp[n.second] > dp[c.second] + n.first) {
                    dp[n.second] = dp[c.second] + n.first;
                    // 更新出来たら次に見るノードの候補に入れる
                    pq.emplace(dp[n.second], n.second);
                }
            }
        }
        rep(i,n) {
            if (sn == i) ans[dp[i]] += 2;
            ans[dp[i]]++;
        }
    }
    rep(i,n-1) cout << (ans[i+1] / 2) << endl;
    
    return 0;
}