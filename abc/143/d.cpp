#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n;
    cin >> n;
    vector<int> l(n);
    rep(i,n) cin >> l[i];
    sort(l.begin(), l.end());
    int l1, l2, l3, num;
    ll ans = 0;
    rep(i,n-2) {
        l1 = l[i];
        for(int j = i+1; j < n-1; j++) {
            l2 = l[j];
            l3 = l1 + l2;
            num = lower_bound(l.begin(), l.end(), l3) - l.begin();
            num = max(num-j-1, 0);
            ans += num;
        }
    }
    cout << ans << endl;
    return 0;
}