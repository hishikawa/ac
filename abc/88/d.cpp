#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <cstdio>
#include <queue>
#define rep(i,n) for (int i = 0; i < n; ++i)
typedef long long ll;

const int INF = 1001001001;

using namespace std;

struct Pos {
    int x, y, step;
    Pos(int x_, int y_, int step_) {
        x = x_; y = y_; step = step_;
    }
};

bool dis(int w, int h, int nx, int ny) {
    if (nx < 0) return false;
    else if (nx >= w) return false;
    else if (ny < 0) return false;
    else if (ny >= h) return false;
    return true;
}

int main() {
    int h, w;
    cin >> h >> w;
    vector<vector<char>> m(h, vector<char>(w));
    int white = 0;
    rep(i,h) {
        rep(j,w) {
            cin >> m[i][j];
            if (m[i][j] == '.') white++;
        }
    }

    queue<Pos> q;
    q.emplace(0, 0, 0);
    int nx, ny, ns;
    int dir[4][2] = {{0,1},{0,-1},{1,0},{-1,0}};

    while (!q.empty()) {
        for (auto d: dir) {
            nx = q.front().x + d[0];
            ny = q.front().y + d[1];
            ns = q.front().step + 1;
            if (nx == w-1 && ny == h-1) {
                cout << white - ns - 1 << endl;
                return 0;
            } else if (dis(w, h, nx, ny)) {
                if (m[ny][nx] == '.') {
                    q.emplace(nx, ny, ns);
                    m[ny][nx] = '#';
                }
            }
        }
        q.pop();
    }
    cout << "-1" << endl;
    return 0;
}