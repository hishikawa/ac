#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    string s;
    cin >> s;
    int q;
    cin >> q;
    int t;
    bool flag = true;
    int count = 0;
    string front = "", back = "";
    rep(i,q) {
        cin >> t;
        if (t == 1) {
            count++;
        } else {
            int f;
            char c;
            cin >> f >> c;
            if (f == 1) {
                if (flag) front.push_back(c);
                else back.push_back(c);
                
            } else {
                if (flag) back.push_back(c);
                else front.push_back(c);
            }
        }
        flag = (count%2 == 0);
    }
    reverse(front.begin(), front.end());
    s = front + s + back;
    if (!flag) reverse(s.begin(), s.end());
    cout << s << endl;
    return 0;
}