#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <cstdio>
#define rep(i,n) for (int i = 0; i < n; ++i)
const int INF = 1001001001;

using namespace std;

int main() {
    int n;
    cin >> n;

    vector<int> p(n), q(n);
    rep(i,n) cin >> p[i];
    rep(i,n) cin >> q[i];

    vector<int> per(n);
    rep(i,n) per[i] = i+1;
    auto ptoi = [n](vector<int> v) {
        int x = 0;
        int mul = 1;
        int l;
        rep(i,n) {
            l = v.size();
            x += mul * v[l-1-i];
            mul *= 10;
        }
        return x;
    };

    int pi = ptoi(p);
    int qi = ptoi(q);
    int cntp = 0, cntq = 0;
    int peri;

    do {
        peri = ptoi(per);
        if (peri <= pi) cntp++;
        if (peri <= qi) cntq++;
    } while (next_permutation(per.begin(), per.end()));

    cout << abs(cntp - cntq) << endl;
    return 0;
}