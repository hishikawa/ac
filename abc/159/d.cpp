#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n;
    cin >> n;
    vector<int> a(n);
    rep(i,n) cin >> a[i];
    map<int,ll> mp;
    rep(i,n) mp[a[i]]++;
    ll total = 0;
    for (auto m: mp) total += m.second * (m.second - 1) / 2;
    rep(i,n) {
        ll pl = max(0LL, ((mp[a[i]] - 1) * (mp[a[i]] - 2) / 2));
        ll mi = (mp[a[i]] - 1) * mp[a[i]] / 2;
        cout << (total - mi + pl) << endl;
    }
    return 0;
}