#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int bitCount(int n) {
    int ans = 0;
    while (n >= 1) {
        if (n & 1) ans++;
        n >>= 1;
    }
    return ans;
}

int main() {
    fast_io
    int h, w, k;
    cin >> h >> w >> k;
    vector<string> s(h);
    rep(i,h) cin >> s[i];

    int ans = INF;
    rep(i,1<<(h-1)) {
        int cuts = bitCount(i);
        vector<vector<int>> white(cuts + 1, vector<int>(w));
        bool ok = true;
        rep(j,w) {
            if (!ok) break;
            int index = 0;
            white[0][j] = s[0][j] - '0';
            rep(l,h-1) {
                if (i & 1 << l) index++;
                white[index][j] += s[l+1][j] - '0';
                if (white[index][j] > k) {ok = false; break;}
            }
        }
        
        vector<int> white_tmp(cuts+1);
        int ans_tmp = cuts;
        rep(j,w-1) {
            rep(l,cuts+1) {
                white_tmp[l] += white[l][j];
                if (white_tmp[l] + white[l][j+1] > k) {
                    white_tmp = vector<int>(cuts+1);
                    ans_tmp += 1;
                    break;
                }
            }
        }
        ans = min(ans_tmp, ans);
    }
    cout << ans << endl;
    return 0;
}