#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

bool dis(string s) {
    string ss = s;
    reverse(s.begin(), s.end());
    if (s == ss) return true;
    else return false;
}

int main() {
    fast_io
    string s;
    cin >> s;
    int n = s.size();
    if (dis(s) && dis(s.substr(0,(n-1)/2)) && dis(s.substr((n+1)/2, (n-1)/2))) {
        cout << "Yes" << endl;
        return 0;
    }
    cout << "No" << endl;
    return 0;
}