#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n, m;
    cin >> n >> m;

    vector<vector<int>> s(m);
    int temp;
    int k;
    for (int i = 0; i < m; ++i) {
        cin >> k;
        for (int j = 0; j < k; ++j) {
            cin >> temp;
            s[i].push_back(temp);
        }
    }

    vector<int> p(m);
    for (int i = 0; i < m; ++i) {
        cin >> p[i];
    }
    
    bool flag;
    int count;
    int ans = 0;
    for (int i = 0; i < (1 << n); ++i) {
        flag = true;
        for (int j = 0; j < m; ++j) {
            if (!flag) break;
            count = 0;
            for (auto sw: s[j]) if ((i>>(sw-1)) & 1) count++;
            if (!(count % 2 == p[j])) {
                flag = false;
                break;
            }
        }
        if (flag) ans++;
    }

    cout << ans << endl;
    return 0;
}