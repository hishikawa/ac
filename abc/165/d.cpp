#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int a;
    long long b, n, x, ans;
    cin >> a >> b >> n;
    x = min(n, b-1);

    ans = double(x) / b * a;

    cout << ans << endl;
    return 0;
}