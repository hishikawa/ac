#include <iostream>

using namespace std;

int main() {
    int k, a, b;
    cin >> k >> a >> b;

    for (int i = 1; i < 1000; ++i){
        if (a <= i*k && i*k <= b) {
            cout << "OK" << endl;
            return 0;
        }
    }
        
    cout << "NG" << endl;
    return 0;
}