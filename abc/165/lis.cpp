#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    const int INF = 1001001001;
    vector<int> a= {2, 45, 6, 5, 6, 3, 7, 8, 3, 20, 9, 11};

    vector<int> dp(a.size() + 1, INF);
    
    dp[0] = -INF;
    for (int i = 0; i < a.size(); ++i) {
        *lower_bound(dp.begin(), dp.end(), a[i]) = a[i];
    }
    // int i = distance(dp.begin(), dp.begin());
    // cout << i << endl << a.size() << endl;
    int i = distance(dp.begin(), lower_bound(dp.begin(), dp.end(), INF)) - 1;
    cout << i << endl;
    return 0;
}