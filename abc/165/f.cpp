#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int INF = 1001001001;
vector<int> a;
vector<vector<int>> to;
vector<bool> seen;
vector<int> dp;
vector<int> ans;

// 深さ優先探索で最長増加部分列LISの長さを求める
void dfs(int start) {
    // 今回のノードで更新するdpのindexとその更新前の値
    int index, old;
    index = distance(dp.begin(), lower_bound(dp.begin(), dp.end(), a[start]));
    old = dp[index];
    //dpの更新
    dp[index] = a[start];
    //この時の最長増加部分列の長さをans配列にためる
    ans[start] = distance(dp.begin(), lower_bound(dp.begin(), dp.end(), INF)) - 1;
    // このノードを見たことを記録    
    seen[start] = true;
    // 一つ深い階層へ再帰
    for (int next : to[start]) {
        // 見たことない、すなわち深い階層なら進む
        if (!seen[next]) dfs(next);
    }
    // 深い階層から帰ってきたら今回のノードで更新したdpを元に戻す
    dp[index] = old;
    return;
}
int main() {
    // 頂点の数
    int n;
    cin >> n;
    // 頂点に書かれてる数字
    a = vector<int>(n);
    for (int i = 0; i < n; ++i) cin >> a[i];
    // つながっている頂点
    to = vector<vector<int>>(n);
    for (int i = 0; i < n - 1; ++i){
        int u, v;
        cin >> u >> v;
        u--; v--;
        to[u].push_back(v);
        to[v].push_back(u);
    }
    // dfs
    seen = vector<bool>(n);
    dp = vector<int>(n + 1, INF);
    dp[0] = -INF;
    ans = vector<int>(n);
    dfs(0);

    for (int i = 0; i < n; ++i) cout << ans[i] << endl;
    return 0;
}