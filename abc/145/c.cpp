#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <cstdio>
#define rep(i,n) for (int i = 0; i < n; ++i)
const int INF = 1001001001;

using namespace std;

int main() {
    int n;
    cin >> n;
    
    vector<pair<int,int>> p;
    int x, y;
    rep(i,n) {
        cin >> x >> y;
        p.emplace_back(x, y);
    }

    auto dist = [](pair<int,int> p1, pair<int,int> p2) {
        double dx = p1.first - p2.first;
        double dy = p1.second - p2.second;
        return sqrt(dx*dx + dy*dy);
    };

    vector<int> per(n);
    rep(i,n) per[i] = i;
    double ans = 0;
    int count = 0;
    do {
        rep(i, n-1) {
            ans += dist(p[per[i]], p[per[i+1]]);
        }
        count++;
    } while (next_permutation(per.begin(), per.end()));
    printf("%.10f\n", ans / count);
    return 0;
}