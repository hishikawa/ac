#include <iostream>
#include <vector>
#include <queue>
#define rep(i,n) for (int i = 0; i < n; ++i)

using namespace std;

const int INF = 1001001001;

int main() {
    int r, c;
    cin >> r >> c;
    int sy, sx;
    cin >> sy >> sx;
    --sy; --sx;
    int gy, gx;
    cin >> gy >> gx;
    --gy; --gx;
    vector<vector<int>> m(r, vector<int>(c, INF));
    char temp;
    rep(i,r) {
        rep(j,c) {
            cin >> temp;
            if (temp == '#') m[i][j] = -INF;
        }
    }

    queue<pair<int,int>> q;
    q.emplace(sx, sy);
    int cx, cy, nx, ny;
    cx = cy = nx = ny = 0;
    int dir[4][2] = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}};

    m[sy][sx] = 0;
    while(!q.empty()) {
        cx = q.front().first;
        cy = q.front().second;
        q.pop();
        for (auto d_sub: dir){
            nx = cx + d_sub[0];
            ny = cy + d_sub[1];
            if ((m[ny][nx] != -INF) && (m[ny][nx] == INF)) {
                if ((ny == gy) && (nx == gx)) {
                    cout << m[cy][cx] + 1 << endl;
                    return 0;
                }
                m[ny][nx] = m[cy][cx] + 1;
                q.emplace(nx, ny);
            }
        }
    }
}