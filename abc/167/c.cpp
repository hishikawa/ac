#include <iostream>
#include <vector>
#include <algorithm>
#include <bitset>
#include <sstream>

using namespace std;

int main() {
    const int INF = 1001001001;
    int n, m, x;
    cin >> n >> m >> x;

    vector<int> c(n);
    vector<vector<int>> a(n, vector<int>(m));
    for (int i = 0; i < n; ++i) {
        cin >> c[i];
        for (int j = 0; j < m; ++j) {
            cin >> a[i][j];
        }
    }

    int iter = 1;
    for (int i = 0; i < n; i++) {
        iter *= 2;
    }


    
    int ans = INF;

    for (int i = 0; i < iter; i++) {
        vector<int> u(m);
        int cost = 0;

        stringstream ss;
        ss << bitset<12>(i);
        string s = ss.str();

        for (int j = 0; j < n; j++) {
            if (s[11-j] - '0') {
                for (int k = 0; k < m; ++k) {
                    u[k] += a[j][k];
                }
                cost += c[j];
            }
        }
        int min_u = *min_element(u.begin(), u.end());
        if ( min_u >= x) {
            ans = min(ans, cost);
        }
    }

    if (ans == INF) {
        cout << -1 << endl;
    } else {
        cout << ans << endl;
    }
    return 0;
}