#include <iostream>
#include <vector>

using namespace std;

const int DIV = 998244353;

// aの2^i乗のmod pを計算
vector<long long> pow_a(int a, int digit, int p) {
    vector<long long> v(digit);
    v[0] = (a % p);
    for (int i = 1; i < digit; ++i) {
        v[i] = ((v[i-1] * v[i-1]) % p);
    }
    return v;
}

// aのmod pでの逆元a^(p-2)を計算
long long inv_mod(int a, int p) {
    vector<long long> v;
    int digit = 30;
    v = pow_a(a, digit, p);
    
    long long ans = 1;
    for (int i = 0; i < digit; ++i) {
        if (((p-2) >> i) & 1) {
            ans = ((ans * v[i]) % p);
        }
    }
    return ans;
}

int main() {
    int n, m, k;
    cin >> n >> m >> k;

    // (m-1)^(n-k-1) mod DIVを求める
    long long a_temp = 1;
    for (int i = 0; i < n-k-1; ++i) {
        a_temp = ((a_temp * (m-1)) % DIV);
    }

    // n-1 C i mod DIVを計算
    vector<long long> c(k+1);
    c[0] = 1;
    // (m-1)^(n-k-1+i) mod DIVを計算
    vector<long long> a(k+1);
    a[0] = a_temp;
    for (int i = 1; i < k+1; ++i) {
        c[i] = ((n - i) % DIV);
        c[i] = ((c[i-1] * c[i]) % DIV);
        c[i] = ((c[i] * inv_mod(i, DIV)) % DIV);

        a[i] = ((a[i-1] * (m-1)) % DIV);
    }

    long long ans = 0;
    long long ans_temp = 0;
    for (int i = 0; i <= k; ++i) {
        ans_temp = (((m % DIV) * c[i]) % DIV);
        ans_temp = ((ans_temp * a[k-i]) % DIV);
        ans = ((ans + ans_temp) % DIV);
    }

    cout << ans << endl;
    return 0;
}