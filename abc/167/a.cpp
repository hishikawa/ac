#include <iostream>
#include <string>
using namespace std;

int main() {
    string s, t;
    cin >> s >> t;
    int n = s.size();

    if (s == t.substr(0, n)) cout << "Yes" << endl;
    else cout << "No" << endl;
    
    return 0;
}