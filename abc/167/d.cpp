#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

int main() {
    int n;
    long long k;
    cin >> n >> k;
    vector<int> a(n);
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
        a[i]--;
    }

    vector<int> aa;
    map<int, int> mp;


    int next = 0;
    int index;

    while (1) {
        aa.push_back(next);
        mp[next] = aa.size();
        next = a[next];
        mp[next];
        if (mp[next] != 0) {
            index = mp[next] - 1;
            break;
        }
    }

    int loop = aa.size() - index;

    if (k < aa.size()) {
        cout << (aa[k] + 1) << endl;
    } else {
        k -= index;
        k %= loop;
        cout << (aa[index + k] + 1) << endl;
    }
    return 0;
}