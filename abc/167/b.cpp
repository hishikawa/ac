#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int a, b, c, k;
    cin >> a >> b >> c >> k;

    int aa, bb, cc;
    aa = min(a, k);
    bb = min(b, k-a);
    if (k-a < 0) bb = 0;
    cc = min(c, k - a - b);
    if (k - a - b < 0) cc = 0;
    cout << aa - cc << endl;
    return 0;
}