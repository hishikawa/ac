#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

struct Edge {
    int from, to, cost;
    Edge(int f,int t, int c) {
        from = f;
        to = t;
        cost = c;
    }
    bool operator <(const Edge &e) const {return (cost < e.cost);}
    bool operator >(const Edge &e) const {return (cost > e.cost);}
};

ll kruskal(int v, const vector<Edge> &e) {
    UnionFind tree(v);
    ll ans = 0;
    for (auto edge: e) {
        if (!tree.same(edge.from, edge.to)) {
            ans += edge.cost;
            tree.unite(edge.from, edge.to);
        }
    }
    return ans;
}

int main() {
    fast_io
    int n;
    cin >> n;
    vector<P> x, y;
    int xtmp, ytmp;
    rep(i,n) {
        cin >> xtmp >> ytmp;
        x.emplace_back(xtmp, i);
        y.emplace_back(ytmp, i);
    }
    sort(x.begin(), x.end());
    sort(y.begin(), y.end());

    auto cost = [](int a, int b) {return abs(a - b);};
    
    vector<Edge> e;
    rep(i,n-1) {
        e.emplace_back(x[i].second, x[i+1].second, cost(x[i].first, x[i+1].first));
        e.emplace_back(y[i].second, y[i+1].second, cost(y[i].first, y[i+1].first));
    }
    sort(e.begin(), e.end());
    cout << kruskal(n, e) << endl;
    return 0;
}