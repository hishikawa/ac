#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n, q;
    cin >> n >> q;

    vector<P> rate_kin(n);
    rep(i,n) cin >> rate_kin[i].first >> rate_kin[i].second;

    vector<P> kid_kin(q);
    rep(i,q) cin >> kid_kin[i].first >> kid_kin[i].second;

    // 幼稚園と園児たちのセット
    map<int,set<int>> kin_kid;
    // 幼稚園と最大レート
    map<int, int> kin_max;
    rep(i,n) {
        kin_kid[rate_kin[i].second].insert(i);
        kin_max[rate_kin[i].second] = max(rate_kin[i].first, kin_max[rate_kin[i].second]);
    }
    int ans = INF;
    for (auto k_m: kin_max) ans = min(k_m.second, ans);

    for (auto q: kid_kin) {
        int kid = q.first - 1;
        int rate = rate_kin[kid].first;
        int pre_kin = rate_kin[kid].second;
        int n_kin = q.second;

        kin_kid[pre_kin].erase(kid);
        // もし最大値の子供が抜けたら
        if (kin_max[pre_kin] == rate) {
            // 幼稚園の子供を見る
            int max_tmp = 0;
            for (auto k: kin_kid[pre_kin]) {
                max_tmp = max(max_tmp, rate_kin[k].first);
                kin_max[pre_kin] = max_tmp;
            }
            // 答えの更新
            ans = min(ans, kin_max[pre_kin]);
        }

        kin_kid[n_kin].insert(kid);
        // もし入ってきた子供が最大値になったら
        if (kin_max[n_kin] < rate) {
            // さらにもともとの答えが更新された場合
            if (ans == kin_max[n_kin]) {
                kin_max[n_kin] = rate;
                int n_ans = INF;
                for (auto k_m: kin_max) n_ans = min(n_ans, k_m.second);
                ans = n_ans;
            }
            kin_max[n_kin] = rate;
        }
        cout << ans << endl;
    }
    return 0;
}