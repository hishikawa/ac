#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int x, y;
    cin >> x >> y;
    int min_ashi = x * 2;
    int max_ashi = x * 4;
    if (min_ashi <= y && y <= max_ashi && y % 2 == 0) cout << "Yes" << endl;
    else cout << "No" << endl;
    return 0;
}