#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int x, n;
    cin >> x >> n;
    if (n == 0) {cout << x << endl; return 0;}
    vector<int> p(n);
    rep(i,n) cin >> p[i];
    sort(p.begin(), p.end());
    int min_dif = INF, j = 0, sign;
    rep(i,102) {
        if (p[j] == i) {
            j++;
            continue;
        }
        if (min_dif > abs(i-x)) {
            min_dif = abs(i-x);
            if (i-x == 0) {sign = 0; break;}
            sign = abs(i-x) / (i-x);
        }
        
    }
    cout << x + sign*min_dif << endl;
    return 0;
}