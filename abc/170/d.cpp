#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n;
    cin >> n;
    vector<int> a(n);
    vector<bool> ok(n, true);
    rep(i,n) cin >> a[i];
    sort(a.begin(), a.end());
    if (a[0] == 1) {
        if (a[1] == 1) {cout << 0 << endl; return 0;}
        else {cout << 1 << endl; return 0;}
    }
    rep(i,n) {
        // 割られている
        if (!ok[i]) continue;
        // もしまだ割られていない
        if (ok[i]) {
            if (i < n-1) {
                if (a[i] == a[i+1]) ok[i] = false;
            }
            // 割る数
            int div = a[i];
            while (div <= a[n-1]) {
                int index = lower_bound(a.begin(), a.end(), div) - a.begin();
                while (a[index] == div) {
                    if (index == i) {index++;continue;}
                    ok[index] = false;
                    index++;
                    if (index > n-1) break;
                }
                div += a[i];
            }
        }
    }
    int count = 0;
    rep(i,n) if (ok[i]) count++;
    cout << count << endl;

    return 0;
}