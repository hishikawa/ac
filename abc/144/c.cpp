#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n;
    cin >> n;
    int N = 1000000;
    ll div1, div2;
    for(ll i = 1; i <= N; i++) {
        if (i*i > n) break;
        if (n % i == 0) div1 = max(div1, i);
    }
    div2 = n / div1;
    ll ans = div2 + div1 - 2;
    cout << ans << endl;
    return 0;
}