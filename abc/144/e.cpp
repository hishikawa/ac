#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

struct G {
    ll a, f, cost;
    G(ll a_, ll f_) {
        a = a_;
        f = f_;
        cost = a * f;
    }
    bool operator <(const G &g) const {return cost < g.cost;}
};

int main() {
    fast_io
    int n;
    ll k;
    cin >> n >> k;
    vector<ll> a(n), f(n);
    rep(i,n) cin >> a[i];
    rep(i,n) cin >> f[i];
    sort(a.begin(), a.end());
    sort(f.rbegin(), f.rend());

    vector<G> g;
    rep(i,n) g.emplace_back(a[i], f[i]);
    sort(g.begin(), g.end());

    ll left = 0, right = 1001001001001, mid;
    bool ok;
    rep(i,100) {
        ll k_copy = k;
        mid = (left + right) / 2;
        ok = true;
        for (G gg: g) {
            if (gg.cost <= mid) continue;
            ll k_tmp = (gg.cost - mid + gg.f - 1) / gg.f;
            k_copy -= k_tmp;
            if (k_copy < 0) {ok = false; break;}
        }
        if (ok) right = mid;
        else left = mid;
    }
    cout << right << endl;

    return 0;
}