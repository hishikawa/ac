#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n, m;
    cin >> n >> m;
    vector<int> a(n);
    int total = 0;
    rep(i,n) {
        cin >> a[i];
        total += a[i];
    }
    int count = 0;
    rep(i,n) {
        if (a[i]*4*m < total) continue;
        count++;
    }
    if (count >= m) cout << "Yes" << endl;
    else cout << "No" << endl;

    return 0;
}