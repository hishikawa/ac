#include <iostream>
using namespace std;
int main(){
    int total_s;
    int h,m,s;
    cin >> total_s;
    h = total_s / 3600;
    total_s %= 3600;
    m = total_s / 60;
    total_s %= 60;
    s = total_s;

    cout << h << ":" << m << ":" << s << endl;

    return 0;
}