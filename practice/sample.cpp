#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void change(vector<int> vv){
    sort(vv.begin(), vv.end());
    return;
}

int main(){
    vector<int> v = {3, 1, 5};
    // change(v);
    sort(v.begin(), v.end());
    cout << v[0] << endl;

    return 0;
}