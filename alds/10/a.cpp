#include <iostream>
#include <vector>
#define rep(i,n) for (int i = 0; i < n; ++i)

using namespace std;

int main() {
    int n;
    cin >> n;
    int fib1 ,fib2 ,fib3;
    fib1 = fib2 = fib3 = 1;
    rep(i,n-1) {
        fib3 = fib1 + fib2;
        fib1 = fib2;
        fib2 = fib3;
    }
    cout << fib3 << endl;
    return 0;
}