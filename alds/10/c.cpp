#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#define rep(i,n) for (int i = 0; i < n; ++i)

using namespace std;

const int INF = 1001001001;

int lcs(string &s1, string &s2, int i, int j, vector<vector<int>> &dp) {
    if (dp[i][j] != INF) {
        return dp[i][j];
    } else if (!(i*j)) {
        dp[i][j] = 0;
        return 0;
    } else if (s1[i-1] == s2[j-1]) {
        dp[i][j] = lcs(s1, s2, i-1, j-1, dp) + 1;
        return dp[i][j];
    } else {
        dp[i][j] = max(lcs(s1, s2, i-1, j, dp), lcs(s1, s2, i, j-1, dp));
        return dp[i][j];
    }
}

int main() {
    int q;
    cin >> q;

    vector<string> s(q*2);
    rep(i,2*q) cin >> s[i];
    int l1, l2;

    rep(i, q) {
        l1 = s[i*2].size();
        l2 = s[i*2+1].size();
        vector<vector<int>> dp(l1+1, vector<int>(l2+1, INF));
        lcs(s[i*2], s[i*2+1], l1, l2, dp);
        cout << dp[l1][l2] << endl;
    }
    return 0;
}