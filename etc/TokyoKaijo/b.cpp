#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll a, v, b, w, t;
    cin >> a >> v >> b >> w >> t;
    ll dist = abs(b-a);

    ll x = v*t - w * t;
    if( x - dist >= 0) cout << "YES" << endl;
    else cout << "NO" << endl;
    return 0;
}