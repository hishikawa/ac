#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    int n, k;
    cin >> n >> k;
    vector<int> a(n);
    rep(i,n) cin >> a[i];
    vector<int> imos(n);
    auto xx = [&](int aka, int ichi) {
        imos[max(0,ichi-aka)]++;
        if (n-1 >= ichi+aka+1) imos[ichi+aka+1]--;
        return;
    };

    rep(i,k) {
        rep(j,n) {
            int ichi = j;
            int aka = a[ichi];
            xx(aka, ichi);
        }
        rep(j,n-1) imos[j+1] += imos[j];
        a = imos;
        imos = vector<int>(n);
        int min_aka = INF;
        rep(j,n) min_aka = min(min_aka, a[j]);
        if(min_aka == n) break;
    }

    rep(i,n) cout << a[i] << " ";
    cout << endl;
    return 0;
}