#include <bits/stdc++.h>
#define rep(i,n) for (int i = 0; i < n; ++i)
#define ll long long
#define P pair<int,int>
#define fast_io ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
const int MOD = 1000000007;
const int INF = 2002002002;
const ll LLINF = 9009009009009009009;
using namespace std;

int main() {
    fast_io
    ll n;
    cin >> n;
    vector<ll> a(n+1), node(n+1), total(n+1);
    rep(i,n+1) cin >> a[i];
    if (a[0] > 1) {cout << "-1" << endl; return 0;}
    total[n] = a[n];
    for (int i = n-1; i >= 0; i--) {
        node[i] = total[i+1];
        total[i] = node[i] + a[i];
    }
    ll reduce;
    total[0] = 1;
    node[0] = total[0] - a[0];
    for (int i = 0; i < n; i++) {
        reduce = max(total[i+1] - node[i]*2, 0LL);
        node[i+1] -= reduce;
        if (node[i+1] < 0) {cout << "-1" << endl; return 0;}
        total[i+1] = node[i+1] + a[i+1];
    }
    ll ans = 0;
    rep(i,n+1) ans += total[i];
    cout << ans << endl;
    return 0;
}